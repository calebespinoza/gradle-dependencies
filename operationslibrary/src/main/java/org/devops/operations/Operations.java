package org.devops.operations;

public class Operations {
    public float Add (float n1, float n2) { return n1 + n2;}
    public float Subtract (float n1, float n2) { return n1 - n2;}
    public float Multiply (float n1, float n2) { return n1 * n2;}
    public float Divide (float n1, float n2) { return n1 / n2;}
    public float Average (float n1, float n2, float n3) { return (n1 + n2 + n3) / 3;}
}
