package calculator;

import org.devops.operations.Operations;

public class Calculator {
    public static void main(String[] args){
        Operations op = new Operations();

        System.out.println("ADD: " + op.Add(12, 12));
        System.out.println("SUBTRACT: " + op.Subtract(122, 345));
        System.out.println("MULTIPLY: " +  op.Multiply(234, 355));
        System.out.println("DIVIDE: " +  op.Divide(123, 34));
        System.out.println("AVERAGE: " + op.Average(10, 45, 21));
    }
}
